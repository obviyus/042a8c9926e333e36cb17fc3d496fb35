import json
import os
from datetime import datetime
from shutil import copyfile

json_files = [json_file for json_file in os.listdir(
    '.') if json_file.endswith('json')]

for each_file in json_files:
    with open(each_file) as json_file:
        data = json.load(json_file)

        location = data["address"]
        date = int(str(data["date_journal"])[:-3])
        readable_date = datetime.utcfromtimestamp(date).strftime('%Y-%m-%d')

        try:
            photo = data["photos"][0]
            copyfile(photo, f"converted/{readable_date}.jpeg")
        except IndexError:
            photo = None

        temperature = data["weather"]["degree_c"]

        content = data["text"]

        with open(f"converted/{str(readable_date)}.md", "w") as f:
            f.write(f"# {str(readable_date)}")
            f.write(f"\n\n{temperature}° C - {location}")

            if photo:
                f.write(
                    f"\n\n![{str(readable_date)}]({str(readable_date)}.jpeg)\n\n")

            f.write(content)
